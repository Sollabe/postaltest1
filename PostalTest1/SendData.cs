﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace PostalTest1
{
    [Activity(Label = "SendData")]
    public class SendData : Activity
    {
        Button InsertButton;
        Button QueryButton;
        EditText TestComment;
        TextView ViewQuery;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Main);
            InsertButton = FindViewById<Button>(Resource.Id.InsertButton);
            TestComment = FindViewById<EditText>(Resource.Id.TestComment);

            InsertButton.Click += InsertClicked;
        }

        private void InsertClicked(object sender, EventArgs e)
        {
            WebClient client = new WebClient();
            Uri uri = new Uri("localhost:/D$/Program Files (x86)/Firebird 3/Test.php");
            NameValueCollection parameters = new NameValueCollection();

            parameters.Add("TestComment", TestComment.Text);

            client.UploadValuesCompleted += Client_UploadValuesCompleted;
            client.UploadValuesAsync(uri, parameters);
        }

        private void Client_UploadValuesCompleted(object sender, UploadValuesCompletedEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
